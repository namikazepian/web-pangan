import { Route, Routes } from 'react-router-dom';
import NavbarComponent from './components/Navbar';
import Home from './container/Home';
import About from './container/About';

const App = () => {
  return (
    <>
      <NavbarComponent />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/about" element={<About />} />
      </Routes>
    </>
  );
};

export default App;
