import { useState } from "react"
import styled from "styled-components"
import Carrousels from "../../components/Carrousels"
import { Container } from "react-bootstrap"
import ContentSection from "../../components/ContentSection"
import AvgProvincePrice from "./AvgProvincePrice"

const Home = () => {
  return (
    <HomeWrapper>
      <Carrousels />
      <Container className="" fluid="sm">
        <ContentSection title={<>Harga Rata-Rata<br />Provinsi</>}>
          <AvgProvincePrice />
        </ContentSection>
      </Container>
    </HomeWrapper>
  )
}

export default Home

const HomeWrapper = styled.div`
`