import React from 'react';
import { Col, Row, Table } from 'react-bootstrap';
import styled from 'styled-components';
import { Select, DatePicker, Button, Dropdown, Space } from 'antd';
import { DownOutlined, UserOutlined } from '@ant-design/icons';
import ContentVisualData from '../../components/ContentVisualData';
import dataTable from '../../data/provinsi';

const AvgProvincePrice = () => {
  const handleButtonClick = (e) => {
    message.info('Click on left button.');
    console.log('click left button', e);
  };

  const handleMenuClick = (e) => {
    message.info('Click on menu item.');
    console.log('click', e);
  };

  const onChange = (date, dateString) => {
    console.log(date, dateString);
  };


  const items = [
    {
      label: 'Pedagang Eceran',
      key: '1',
    },
    {
      label: 'Pedagang Grosir',
      key: '2',
    },
    {
      label: 'Produsen',
      key: '3',
      // danger: true,
    },
  ];
  const menuProps = {
    items,
    onClick: handleMenuClick,
  };
  return (
    <ContentVisualData>
      <Row>
        <Col lg>
          <Dropdown menu={menuProps}>
            <Button block style={{ display: "flex", justifyContent: "space-between" }}>
              <div>Button</div>
              <div><DownOutlined /></div>
            </Button>
          </Dropdown>
        </Col>
        <Col lg>
          <Select
            showSearch
            style={{ width: "100%" }}
            placeholder="Cari Komoditas"
            optionFilterProp="children"
            filterOption={(input, option) => (option?.label.toLowerCase() ?? '').includes(input)}
            filterSort={(optionA, optionB) => (optionA?.label ?? '').toLowerCase().localeCompare((optionB?.label ?? '').toLowerCase())}
            options={[
              {
                value: '1',
                label: 'Beras Premium',
              },
              {
                value: '2',
                label: 'Beras Medium',
              },
              {
                value: '3',
                label: 'Bawang Merah',
              },
            ]}
          />
        </Col>
        <Col lg className="date-picker-col" style={{ position: "relative" }}>
          <DatePicker
            style={{ width: "100%" }}
            onChange={onChange}
            placement="bottomLeft"
            popupClassName="datepicker-dropdown-custom"
            getPopupContainer={() => document.querySelector('.date-picker-col')}
          />
        </Col>
      </Row>
      <Row className="mt-2">
        <Col lg={5} className="table-custom-container">
          <Table className="table-custom">
            <thead id="thead-custom" style={{ backgroundColor: "#f7f7f7 !important" }}>
              <tr>
                <th>Provinsi</th>
                <th>Harga</th>
                <th>Disparitas</th>
              </tr>
            </thead>
            <tbody>
            {dataTable.map(data => (
              <tr key={data.id}>
                <th>{data.provinsi}</th>
                <td>{data.harga}</td>
                <td>{data.disparitas}</td>
              </tr>
            ))}
            </tbody>
          </Table>
        </Col>
        <Col lg={7}></Col>
      </Row>
    </ContentVisualData>
  );
};

export default AvgProvincePrice;

const OptionItem = styled.option``;
