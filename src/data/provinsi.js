const dataTable = [
  {
    id: 1,
    provinsi: "Aceh",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
  {
    id: 2,
    provinsi: "Sumatera Utara",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
  {
    id: 3,
    provinsi: "Sumatera Barat",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
  {
    id: 4,
    provinsi: "Riau",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
  {
    id: 5,
    provinsi: "Jambi",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
  {
    id: 6,
    provinsi: "Sumatera Selatan",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
  {
    id: 7,
    provinsi: "Bengkulu",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
  {
    id: 8,
    provinsi: "Lampung",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
  {
    id: 9,
    provinsi: "Kepulauan Bangka Belitung",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
  {
    id: 10,
    provinsi: "Kepulauan Riau",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
  {
    id: 11,
    provinsi: "DKI Jakarta",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
  {
    id: 12,
    provinsi: "Jawa Barat",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
  {
    id: 13,
    provinsi: "Jawa Tengah",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
  {
    id: 14,
    provinsi: "D.I Yogyakarta",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
  {
    id: 15,
    provinsi: "Jawa Timur",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
  {
    id: 16,
    provinsi: "Banten",
    harga: "Rp14.000",
    disparitas: "-6%"
  },
]

export default dataTable