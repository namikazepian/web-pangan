import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import LogoRight from '../assets/images/bpn-logo.png'
import LogoLeft from '../assets/images/php-logo.png'
import styled from 'styled-components';

function BasicExample() {
  return (
    <Navbar expand="lg" className="bg-body-tertiary">
      <Container>
        <Navbar.Brand href="#home">
          <BrandInfo>
            <LogoImageRight src={LogoRight} alt="bpn-logo" className="d-inline-block align-top" />
            <TextWrapper>
              <LogoText>DIREKTORAT STABILISASI PASOKAN DAN HARGA PANGAN<br/>KEDEPUTIAN BIDANG KETERSEDIAAN DAN STABILISASI<br/>PANGAN</LogoText>
            </TextWrapper>
          </BrandInfo>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse className="justify-content-end nav-items-custom" id="basic-navbar-nav">
          <NavButtonWrapper>
            <Nav className="me-auto">
              <NavLinkWrapper>
                <Nav.Link className="nav-link-custom" href="#home">Beranda</Nav.Link>
                <NavButtonDivider />
                <NavDropdown className="nav-link-custom" title="Table Harga" id="basic-nav-dropdown">
                  <NavDropdown.Item className="nav-link-dropdown-custom" href="#action/2.1">Harga Produsen</NavDropdown.Item>
                  <NavDropdown.Item className="nav-link-dropdown-custom" href="#action/2.2">Harga Grosir</NavDropdown.Item>
                  <NavDropdown.Item className="nav-link-dropdown-custom" href="#action/2.3">Harga Eceran</NavDropdown.Item>
                </NavDropdown>
                <NavButtonDivider />
              </NavLinkWrapper>
              <NavDropdown className="nav-link-custom" title="Informasi" id="basic-nav-dropdown">
                <NavDropdown.Item className="nav-link-dropdown-custom" href="#action/3.1">FAQ</NavDropdown.Item>
                <NavDropdown.Item className="nav-link-dropdown-custom" href="#action/3.2">Download</NavDropdown.Item>
                <NavDropdown.Item className="nav-link-dropdown-custom" href="#action/3.3">Kontak Kami</NavDropdown.Item>
                <NavDropdown.Item className="nav-link-dropdown-custom" href="#action/3.4">List Survey</NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </NavButtonWrapper>
          <LogoImageLeft src={LogoLeft} alt="php-logo" className="d-inline-block align-top" />
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default BasicExample;

const LogoImageRight = styled.img`
  width: 130px;
`
const LogoImageLeft = styled.img`
  width: 130px;
`
const LogoText = styled.span`
  color: #42692a;
  font-size: 12px;
  font-weight: 700;
  text-decoration: none;
  padding-left: 12px;
  text-align: left;
`
const TextWrapper = styled.div`
  display: flex;
  flex-direction: column;
  border-left: 1px solid #42692a;
  margin-left: 15px;
`
const BrandInfo = styled.div`
  display: flex;
  flex-direction: row;
`
const NavButtonWrapper = styled.div`
  padding: 0 15px;
`
const NavLinkWrapper = styled.div`
  display: flex;
  align-items: center;
`
const NavButtonDivider = styled.div`
  border-left: 1.5px solid #42692a;
  height: 35px;
  margin: 0 10px;
`