import { useState } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import CarouselImage1 from '../assets/images/carousel-a.jpg';
import CarouselImage2 from '../assets/images/carousel-b.jpg';
import CarouselImage3 from '../assets/images/carousel-c.jpg';
import styled from 'styled-components';

function Carrousels() {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex) => {
    setIndex(selectedIndex);
  };

  return (
    <Carousel activeIndex={index} onSelect={handleSelect} indicators={false}>
      <Carousel.Item>
        <CarrouselsImage src={CarouselImage1} alt="carousel-selamat-datang" className="d-inline-block align-top" />
        <Carousel.Caption>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
      <CarrouselsImage src={CarouselImage2} alt="carousel-play-store" className="d-inline-block align-top" />
        <Carousel.Caption>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
      <CarrouselsImage src={CarouselImage3} alt="carousel-stabilitas-pangan" className="d-inline-block align-top" />
        <Carousel.Caption>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default Carrousels;

const CarrouselsImage = styled.img`
  width: 100%;
  height: 420px;
  object-fit: cover;
`