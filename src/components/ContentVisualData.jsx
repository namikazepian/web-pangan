import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import styled from 'styled-components';

function ContentVisualData({ title, children }) {
  return (
    <CardContainer>
      <Card className="card-custom">
        <CardWrapper>
          <Card.Body>
              <Card.Title>
                <CardTitleStyled>
                  {title}
                </CardTitleStyled>
              </Card.Title>
              <Card.Text>
                {children}
              </Card.Text>
          </Card.Body>
        </CardWrapper>
      </Card>
    </CardContainer>
  );
}

export default ContentVisualData;

const CardContainer = styled.div`
  margin-top: 1.5rem;
  font-size: 14px !important;
`
const CardWrapper = styled.div`
  width: 100%;
`
const CardTitleStyled = styled.div`
  font-size: 1.4em;
  margin-bottom: .5em;
`


