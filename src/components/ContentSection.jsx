import { Container } from 'react-bootstrap'
import styled from 'styled-components'

const ContentSection = ({ title, children }) => {
  return (
    <SectionWrapper>
      <SectionHeaderTitle>{title}</SectionHeaderTitle>
      {children}
    </SectionWrapper>
  )
}

export default ContentSection

const SectionWrapper = styled.div`
  /* background-color: #f7f7f7;
  background-image: url(../assets/img/background-section22.png);
  background-position: right top;
  background-repeat: no-repeat; */
  /* background-size: auto 110%; */
  width: 100%;
  margin-bottom: 2rem;
`
const SectionHeaderTitle = styled.h1`
    color: #629153;
    font-weight: 800;
    font-size: 1.9rem;
    padding-top: 2rem;
`