import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import styled from 'styled-components';

function ContentCard({ title, children }) {
  return (
    <CardContainer>
      <Card className="card-custom">
        <CardWrapper>
          <Card.Body>
            <ContentWrapper>
              <Card.Title>
                <CardTitleStyled>
                  {title}
                </CardTitleStyled>
              </Card.Title>
              <Card.Text>
                {children}
              </Card.Text>
            </ContentWrapper>
          </Card.Body>
        </CardWrapper>
      </Card>
    </CardContainer>
  );
}

export default ContentCard;

const CardContainer = styled.div`
  margin-top: 1.5rem;
`
const CardWrapper = styled.div`
  width: 100%;
`
const ContentWrapper = styled.div`
  padding: 42px;
`
const CardTitleStyled = styled.div`
  font-size: 1.4em;
  margin-bottom: .5em;
`


